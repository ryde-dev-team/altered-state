'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Audio
	Field filename:String
	Field sound:TSound
	Field channel:TChannel
	
	Function Load:Audio(fn:String, looping = False)
		Local ta:Audio = New Audio
		ta.filename = fn
		ta.sound = LoadSound(ta.filename, looping)
		If ta.sound = Null
			RuntimeError("Cant find audio " + ta.filename)
		End If
		ta.channel = CueSound(ta.sound)
		Return ta
	End Function
	
	Method Play()
		ResumeChannel channel
	End Method
	
	Method Stop()
		StopChannel channel
	End Method

End Type



