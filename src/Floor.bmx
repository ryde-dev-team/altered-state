'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Floor
	Field floorType
	Field floorID
	Field density
	Field roomList:TList = New TList
	Field currentRoom:Room
	Field music1:Audio = Audio.Load(CurrentDir() + "/media/music/Altered State/Nocturne.wav", True)
	
	Field lastRoomSpawn:Room = Null
	
	
	Function Load:Floor(floorID)
		Cls
		Local loadTex:Text = text.Load("Loading Floor", 0, 0)
		loadTex.Draw()
		Flip
		Local tmpFloor:Floor = New Floor
		If floorID = FLOORID_COURTYARD
			tmpFloor.floorType = FLOORTYPE_COURTYARD
		End If
		tmpFloor.density = Rnd(1, 10)
		
		tmpFloor.addRoom(0, ROOMTYPE_SPAWN)
		Local i
		For i = 1 To tmpFloor.density
			tmpFloor.addRoom(i)
		Next
		
		tmpFloor.addRoom(i + 1, ROOMTYPE_KEY)
		tmpFloor.addRoom(i + 2, ROOMTYPE_BOSS)
		
		tmpFloor.music1.Play()
		Return tmpFloor
	End Function
	
	Method addRoom(rid, roomType = 0)
		If roomType = 0
			roomType = Rnd(ROOMTYPE_MONSTER, ROOMTYPE_RESOURCE)
		End If
		Local tmpRoom:Room = Room.Load(rid, roomType)
		
		
		If Not rid = 0
			Local xTry:Double = lastRoomSpawn.oX
			Local yTry:Double = lastRoomSpawn.oY
			
			Local xDir:Double = 1 * Rnd(-1, 1)
			Local yDir:Double = 1 * Rnd(-1, 1)
			
			tmpRoom.oX = xTry + xDir
			tmpRoom.oY = yTry + yDir
			Repeat
				If checkRoomPlacement(tmpRoom.oX, tmpRoom.oY, tmpRoom.dimX, tmpRoom.dimY) = True
					Exit
				EndIf
				
				xTry = tmpRoom.oX
				yTry = tmpRoom.oY
				
				Local which = Rand(0,1)
				
				Select which
					Case 0
						tmpRoom.oX = xTry + xDir
					Case 1
						tmpRoom.oY = yTry + yDir
				End Select
				
			Forever
			
			
		EndIf
		
		tmpRoom.rec.X = tmpRoom.oX
		tmpRoom.rec.Y = tmpRoom.oY
		
		roomList.AddLast(tmpRoom)
		lastRoomSpawn = tmpRoom
		
	
	End Method
	
	Method Update()
		For Local tmpRoom:Room = EachIn roomList
			tmpRoom.Update()
		Next
		
		'Draw Debug Text
		
		For Local tmpRoom:Room = EachIn roomList
			SetColor 255, 255, 255
			SetAlpha 1
			tmpRoom.debugText.Draw()
			tmpRoom.debugText = Null
		Next
		
		For Local tmpRoom:Room = EachIn roomList
			If RectsOverlap(alteredState.mainPlayer.X, alteredState.mainPlayer.Y, 10, 10, tmpRoom.ox, tmpRoom.oY, tmpRoom.dimX, tmpRoom.dimY)
				currentRoom = tmpRoom
				Exit
			EndIf
		Next
		
		If currentRoom <> alteredState.mainPlayer.currentRoom
			alteredState.mainPlayer.currentRoom = currentRoom
			currentRoom.a = 1
		EndIf
		
		
	End Method
	
	Method checkRoomPlacement(tmpx, tmpy, tmpdimX, tmpdimY)
		If tmpx = 0 And tmpy = 0
			Return False
		End If
		
		Local tlx = tmpx
		Local tly = tmpY
		
		
		
		For Local tmpRoom:Room = EachIn roomList
			
			Local tmptlx = tmpRoom.oX
			Local tmptly = tmpRoom.oY
			
			
			If RectsOverlap(tlx, tly, tmpdimX, tmpdimY, tmptlx, tmptly, tmpRoom.dimX, tmpRoom.dimY)
				Return False
			End If
			
			
			
		Next
		
		Return True
	End Method
End Type
