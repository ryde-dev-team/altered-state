'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Global alteredState:Game = New Game

Type Game
	Field nam:String = "Altered State"
	Field version:String = "0.1"
	Field mode
	Field saveFile:String = CurrentDir() + "/data/save.dat"
	
	Field mainPlayer:Player
	Field currentLevel:Level
	
	Method Load()
		Nam = "Altered State"
		version = "0.1"
		AppTitle = Nam + " " + version
		Print "Loading Main Menu"
		Local mainMenu:MainMenu = mainMenu.Load(FileType(saveFile))
		Print "Running Main Menu"
		mainMenu.menuMusic.Play()
		While mainMenu.run()
			Select mainMenu.choice
				Case MAINMENU_PLAY
					mode = GAMEMODE_PLAY
					mainPlayer = Player.Load()
					currentLevel = Level.Load()
					
					'set params for continue game and exit to game loop
					Print "Play Selected"
					Exit
				Case MAINMENU_NEW
					Notify "This option has not been implemented"
					mainMenu.choice = 0
					'mode = GAMEMODE_NEW
					'set params for new game and exit to game loop
					'Print "New Selected"
					
					'Exit
				Case MAINMENU_OPTIONS
					Notify "This option has not been implemented"
					mainMenu.choice = 0
					'Load and run options menu
					'Print "Options Selected"
				Case MAINMENU_EXIT
					Print "Kill Selected"
					End
			End Select
		Wend
		FlushKeys
		MainMenu.menuMusic.Stop()
	End Method
	
	Method run()
		Cls
		WaitTimer(appClock)
		currentLevel.Update()
		If KeyHit(KEY_1)
			mainPlayer.X = 0
			mainPlayer.Y = 0
			currentLevel.currentFloor.music1.Stop()
			currentLevel = Level.Load()
			mainPlayer.currentLevel = currentLevel
			currentLevel.currentFloor.music1.Play()
		End If
		mainPlayer.Update()
		updateFPS()
		Flip
		
		Return True
	End Method
	
	Method Kill()
		
		End
	End Method
End Type
