'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Player
	Field uName:String = "Player"
	Field health = 10
	Field maxHealth = 10
	Field X = 0
	Field Y = 0
	
	Field currentLevel:Level
	Field currentFloor:Floor
	Field currentRoom:Room
	
	Function Load:Player()
		Local tmpPlayer:Player = New Player
		Return tmpPlayer
	End Function
	
	Method Update()
		UpdateMovement()
		Self.render()
	End Method
	
	Method UpdateMovement()
		
		
		If KeyDown(KEY_W)
			Self.Y = Self.Y - 1
			
			If Not RectsOverlap(alteredState.mainPlayer.X, alteredState.mainPlayer.Y, 10, 1, currentRoom.oX, currentRoom.oY, currentRoom.dimX, currentRoom.dimY)
				Self.Y = Self.Y + 1
			End If
		End If
		If KeyDown(KEY_S)
			Self.Y = Self.Y + 1
			
			If Not RectsOverlap(alteredState.mainPlayer.X, alteredState.mainPlayer.Y + 10, 10, 1, currentRoom.oX, currentRoom.oY, currentRoom.dimX, currentRoom.dimY)
				Self.Y = Self.Y - 1
			End If
		End If
		If KeyDown(KEY_A)
			Self.X = Self.X - 1
			
			If Not RectsOverlap(alteredState.mainPlayer.X, alteredState.mainPlayer.Y, 1, 10, currentRoom.oX, currentRoom.oY, currentRoom.dimX, currentRoom.dimY)
				Self.X = Self.X + 1
			End If
		End If
		If KeyDown(KEY_D)
			Self.X = Self.X + 1
			
			If not RectsOverlap(alteredState.mainPlayer.X + 10, alteredState.mainPlayer.Y, 1, 10, currentRoom.oX, currentRoom.oY, currentRoom.dimX, currentRoom.dimY)
				Self.X = Self.X - 1
			End If
		EndIf
	End Method
	
	Method render()
		SetColor 0, 0, 255
		SetAlpha 1
		DrawRect(0, 0, 10, 10)
	End Method
End Type
