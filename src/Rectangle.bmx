'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Rectangle
	Field solid
	Field X:Double, Y:Double, w:Double, h:Double
	Field r, g, b
	Field alpha:Float
	
	Function Load:Rectangle(X, Y, w, h, r, g, b, a = 1, s = True)
		Local tmpRec:Rectangle = New Rectangle
		tmpRec.solid = s
		tmpRec.X = X
		tmpRec.X = Y
		tmpRec.w = w
		tmpRec.h = h
		tmpRec.r = r
		tmpRec.g = g
		tmpRec.b = b
		tmpRec.alpha = a
		
		
		Return tmpRec
	End Function
	
	Method Update()
		SetColor r, g, b
		SetAlpha alpha
		DrawRect(X - alteredState.mainPlayer.X, Y - alteredState.mainPlayer.Y, w, h)
	End Method
End Type


Function RectsOverlap:Int(x0:Float, y0:Float, w0:Float, h0:Float, x2:Float, y2:Float, w2:Float, h2:Float)
	If x0 > (x2 + w2) Or (x0 + w0) < x2 Then Return False
	If y0 > (y2 + h2) Or (y0 + h0) < y2 Then Return False
	Return True
End Function