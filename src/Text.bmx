'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem


Type Text
	Field str:String = ""
	Field X = 0, Y = 0
	Field hidden = False
	Field centered = True
	
	Field r = 255, g = 255, b = 255
	Field rot = 0
	Field scaleX = 1, scaleY = 1
	
	Function Load:Text(tmpstr:String, tmpx, tmpy, cent = True)
		Local tmptxt:Text = New Text
		tmptxt.str = tmpstr
		tmptxt.centered = cent
		If cent
			tmptxt.X = tmpx - (TextWidth(tmpstr) / 2)
		Else
			tmptxt.X = tmpx
		EndIf
		tmptxt.Y = tmpy
		Return tmptxt
	End Function
	
	Method Draw()
		SetColor r, g, b
		SetScale scaleX, scaleY
		SetRotation rot
		DrawText str, X, Y
	End Method
End Type


