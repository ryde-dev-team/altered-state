'This BMX file was edited with BLIde ( http://www.blide.org )

Global appClock:TTimer = CreateTimer(60)

Graphics 800, 600
SetOrigin 400, 300

Global saveFile:String = CurrentDir() + "/data/save.dat"

Global fps:Text = Text.Load("FPS: 0", 300, 200)
Global lastFrame = MilliSecs()
Global currentFrame
Global frameRate

SetBlend ALPHABLEND

alteredState.Load()

While alteredState.run()
	
WEnd

alteredState.Kill()


End



Function updateFPS()
	currentFrame = MilliSecs()
	If (currentFrame - lastFrame) = 0
		lastFrame = currentFrame / 2
	End If
	frameRate = 1000 / (currentFrame - lastFrame)
	fps.str = "FPS: " + frameRate
	lastFrame = currentFrame
	fps.Draw()
End Function

Function loadSave()
	
End Function

Function drawGUI()

End Function