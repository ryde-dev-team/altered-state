'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type MainMenu
	Field choice
	Field X, Y = -90
	Field previousSave
	Field cursor
	Field moveCursor
	Field textList:TList = New TList
	Field menuMusic:Audio = Audio.Load(CurrentDir() + "/media/music/Altered State/Exiled.wav", True)
	
	Function Load:MainMenu(prevSave = False)
		Local tmpMenu:MainMenu = New MainMenu
		tmpMenu.previousSave = prevSave
		tmpMenu.textList.AddFirst(Text.Load("Altered State", tmpMenu.X, tmpMenu.Y))
		tmpMenu.textList.AddFirst(Text.Load("Play", tmpMenu.X, tmpMenu.Y + 60))
		tmpMenu.textList.AddFirst(Text.Load("New", tmpMenu.X, tmpMenu.Y + 60 + 15))
		tmpMenu.textList.AddFirst(Text.Load("Options", tmpMenu.X, tmpMenu.Y + 60 + 15 + 15))
		tmpMenu.textList.AddFirst(Text.Load("Exit", tmpMenu.X, tmpMenu.Y + 60 + 15 + 15 + 15))
		tmpMenu.moveCursor = MAINMENU_PLAY
		Return tmpMenu
	End Function
	
	Method run()
		WaitTimer(appClock)
		SetClsColor(30, 40, 60)
		Cls
		For Local tmpText:Text = EachIn textList
			tmpText.Draw()
		Next
		If moveCursor <> cursor
			Select moveCursor
				Case MAINMENU_PLAY
					textList.clear()
					textList.AddFirst(Text.Load("Altered State", Self.X, Self.Y))
					If previousSave
						textList.AddFirst(Text.Load("[Play]", Self.X, Self.Y + 60))
					EndIf
					textList.AddFirst(Text.Load("New", Self.X, Self.Y + 60 + 15))
					textList.AddFirst(Text.Load("Options", Self.X, Self.Y + 60 + 15 + 15))
					textList.AddFirst(Text.Load("Exit", Self.X, Self.Y + 60 + 15 + 15 + 15))
					cursor = moveCursor
				Case MAINMENU_OPTIONS
					textList.clear()
					textList.AddFirst(Text.Load("Altered State", Self.X, Self.Y))
					If previousSave
						textList.AddFirst(Text.Load("Play", Self.X, Self.Y + 60))
					EndIf
					textList.AddFirst(Text.Load("New", Self.X, Self.Y + 60 + 15))
					textList.AddFirst(Text.Load("[Options]", Self.X, Self.Y + 60 + 15 + 15))
					textList.AddFirst(Text.Load("Exit", Self.X, Self.Y + 60 + 15 + 15 + 15))
					cursor = moveCursor
				Case MAINMENU_NEW
					textList.clear()
					textList.AddFirst(Text.Load("Altered State", Self.X, Self.Y))
					If previousSave
						textList.AddFirst(Text.Load("Play", Self.X, Self.Y + 60))
					EndIf
					textList.AddFirst(Text.Load("[New]", Self.X, Self.Y + 60 + 15))
					textList.AddFirst(Text.Load("Options", Self.X, Self.Y + 60 + 15 + 15))
					textList.AddFirst(Text.Load("Exit", Self.X, Self.Y + 60 + 15 + 15 + 15))
					cursor = moveCursor
				Case MAINMENU_EXIT
					textList.clear()
					textList.AddFirst(Text.Load("Altered State", Self.X, Self.Y))
					If previousSave
						textList.AddFirst(Text.Load("Play", Self.X, Self.Y + 60))
					EndIf
					textList.AddFirst(Text.Load("New", Self.X, Self.Y + 60 + 15))
					textList.AddFirst(Text.Load("Options", Self.X, Self.Y + 60 + 15 + 15))
					textList.AddFirst(Text.Load("[Exit]", Self.X, Self.Y + 60 + 15 + 15 + 15))
					cursor = moveCursor
			End Select
		EndIf
		
		If KeyHit(KEY_DOWN)
			moveCursor = moveCursor + 1
			
			If moveCursor > 103
				moveCursor = 100
			End If
			If Not previousSave And moveCursor = 100
				moveCursor = 101
			End If
		End If
		If KeyHit(KEY_UP)
			moveCursor = moveCursor - 1
			If moveCursor < 100
				moveCursor = 103
			End If
			If Not previousSave And moveCursor = 100
				moveCursor = 103
			End If
		End If
		If KeyHit(KEY_SPACE)
			choice = cursor
		End If
		
		If KeyHit(KEY_ESCAPE)
			choice = MAINMENU_EXIT
		End If
		Flip
		Return True
	End Method
End Type
