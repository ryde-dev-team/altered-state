'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Level
	Field floorList:TList = New TList
	Field currentFloor:Floor
	
	Function Load:Level(seed = 0)
		If seed = 0
			seed = Rnd(-MilliSecs(), MilliSecs())
		End If
		SeedRnd(seed)
		local tmpLevel:Level = New Level
		Local tmpFloor:Floor = Floor.Load(FLOORID_COURTYARD)
		tmpLevel.floorList.AddLast(tmpFloor)
		tmpLevel.currentFloor = tmpFloor
		Return tmpLevel
	End Function
	
	Method Update()
		currentFloor.Update()
		alteredState.mainPlayer.currentFloor = currentFloor
	End Method
End Type
