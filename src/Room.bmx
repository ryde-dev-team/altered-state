'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Room
	Field roomType
	Field roomID
	Field dimX, dimY
	Field oX:Double = 0, oY:Double = 0
	Field density
	Field r, g, b
	Field a:Float
	
	Field portalList:TList = New TList
	
	Field debugText:Text
	
	Field rec:Rectangle
	
	
	
	Function Load:Room(rid, roomType)
		Local tmpRoom:Room = New Room
		tmpRoom.roomType = roomType
		tmpRoom.roomID = rid
		tmpRoom.dimX = Rnd(10, 50) * 4'*10 (10,50)
		tmpRoom.dimy = Rnd(10, 50) * 4'*10
		
		If tmpRoom.roomType = ROOMTYPE_SPAWN
			tmpRoom.ox = tmpRoom.ox - (tmpRoom.dimX / 2)
			tmpRoom.oY = tmpRoom.oY - (tmpRoom.dimY / 2)
		End If
		
		tmpRoom.density = Rnd(1, 10)
		
		tmpRoom.r = 25 * tmpRoom.density
		tmpRoom.g = 255 / tmpRoom.density
		tmpRoom.b = 15
		
		tmpRoom.a =.3
		
		tmpRoom.rec = Rectangle.Load(tmpRoom.ox, tmpRoom.oY, tmpRoom.dimX, tmpRoom.dimy, tmpRoom.r, tmpRoom.g, tmpRoom.b, tmpRoom.a)
		
		Return tmpRoom
	End Function
	
	Method Update()
		rec.alpha = a
		Self.render()
	End Method
	
	Method render()
		rec.Update()
		debugText = Text.Load("Room Type: " + roomType + " Density: " + density, oX - alteredState.mainPlayer.X, oY - alteredState.mainPlayer.Y - 15, False)
		'DrawText("X: " + oX + " Y: " + oY, oX - alteredState.mainPlayer.X, oY - alteredState.mainPlayer.Y - 30)
	End Method
End Type
